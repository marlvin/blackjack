const MDCTextField = mdc.textField.MDCTextField;
const foos = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);

const suits = ["♠", "♡", "♢", "♣"];
const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];

const deck = [];
for (const suit of suits) {
  for (const rank of ranks) {
    deck.push({
      suit, // object literal syntax: short for `suit: suit`
      rank,
    });
  }
}

function getDeck() {
  return deck;
}
function getRandomCard() {
  return deck[Math.floor(Math.random() * deck.length)];
}
function dealRandomCard() {
  dealToDisplay(getRandomCard());
}

const hitButton = document.querySelector("#hit-button");
hitButton.addEventListener("click", () => {
  const randomCard = getRandomCard();
  console.log(
    `Your card is ${randomCard.rank} of ${randomCard.suit}. Did you bust?`
  );
  console.log(randomCard);
});
